var webOrVideo = 'v'; //our work page :: if video is active : value = v / if web is active : value = w
var isAlreadyExecuted = false;
var indexShowreelViewer = 0;
var selectedVignet = -1;
var lastSelectedMenu = "#homeMenuLine";
var isTouchScreenAlreadyActivated = false; // coucou tu veux voir ma grande variable ???
//variables used for annimations and for the ajax sending the simulation
var videoActive = false;
var webActive = false;

$("#site").hide();
$(window).on('load',
	function() {
		$("#loading").hide();
		$("#site").show();
	});
			
window.addEventListener('touchstart', touchScreenDetected);

document.addEventListener("DOMContentLoaded", function() {
  var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));

  if ("IntersectionObserver" in window) {
	let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
	  entries.forEach(function(entry) {
		if (entry.isIntersecting) {
		  let lazyImage = entry.target;
		  lazyImage.src = lazyImage.dataset.src;
		  lazyImage.srcset = lazyImage.dataset.srcset;
		  lazyImage.classList.remove("lazy");
		  lazyImageObserver.unobserve(lazyImage);
		}
	  });
	});
	
	lazyImages.forEach(function(lazyImage) {
	  lazyImageObserver.observe(lazyImage);
	});
  }
});

function touchScreenDetected() {
    if (!isTouchScreenAlreadyActivated) {
        isTouchScreenAlreadyActivated = true;
        RenderSensitiveVignets();
    }
}
// les tableaux de data video et web
// les titres doivent toujours être sur quatre lignes pour être correctement positionnés :
// utiliser      < br />&zwnj;      pour que les lignes vides soient prises en compte dans la mise en page

var propsVignets = { "altCache": "cache", "width": "505", "height": "206" };
var allVignets = [
    { "Type": "v", "Title": "showreel 2019", "alt": "video 1", "srcImage": "./images/vignetteShowreel.png", "srcImageCache": "./images/videoVignetteCache.jpg", "src": ["https://www.youtube.com/embed/0LdhYi0rc9E"],"fontColor":"white", "html": "<h5 id=\"videoVignetteTitle1\">Showreel<br><b class=\"redTitle\">2019<br />&zwnj;<br />&zwnj;</b></h5>" },
    { "Type": "v", "Title": "Heliopsis, Le Pisé", "alt": "video 2", "srcImage": "./images/vignetteHeliopsis.png", "srcImageCache": "./images/videoVignetteCache.jpg", "src": ["https://www.youtube.com/embed/vfoitNemWw8"], "fontColor": "white",  "html": "<h5 id=\"videoVignetteTitle2\">Documentaire<br><b class=\"redTitle\">Heliopsis<br />&zwnj;<br />&zwnj;</b></h5>" },
    { "Type": "v", "Title": "Festival Peyotl 2019", "alt": "video 3", "srcImage": "./images/vignetteFestival.png", "srcImageCache": "./images/videoVignetteCache.jpg", "src": ["https://www.youtube.com/embed/Fy1kndRyARc"], "fontColor": "white",  "html": "<h5 id=\"videoVignetteTitle3\">Aftermovie<br><b class=\"redTitle\">Festival Peyotl 2019<br />&zwnj;<br />&zwnj;</b></h5>" },
    { "Type": "v", "Title": "Stage Sportif", "alt": "video 4", "srcImage": "./images/vignetteStageSportif.jpg", "srcImageCache": "./images/videoVignetteCache.jpg", "src": ["https://www.youtube.com/embed/PS-r155PHgc"],"fontColor":"white", "html": "<h5 id=\"videoVignetteTitle4\">Stage<br><b class=\"redTitle\">Sportif<br />&zwnj;<br />&zwnj;</b></h5>" },
	{ "Type": "w", "Title": "sonopti", "alt": "web 1", "srcImage": "./images/vignetteSonopti.jpg", "src": ["./images/sonopti1.jpg", "./images/sonopti2.jpg"], "srcImageCache": "./images/videoVignetteCache.jpg", "fontColor": "white",  "html": "<h5 id=\"videoVignetteTitle1\">organisateur<br>musical :<br><b class=\"redTitle\">sonopti</b><br>&zwnj;</h5>" },
    { "Type": "w", "Title": "l'atelier libre", "alt": "web 2", "srcImage": "./images/vignetteAtelierLibre.jpg", "src": ["./images/atelierLibre1.jpg", "./images/atelierLibre2.jpg"], "srcImageCache": "./images/videoVignetteCache.jpg", "fontColor": "white", "html": "<h5 id=\"videoVignetteTitle2\">L'atelier<br><b class=\"redTitle\">libre</b><br>&zwnj;<br>&zwnj;</h5>" },
    { "Type": "w", "Title": "interface client", "alt": "web 3", "srcImage": "./images/vignetteClientInterface.jpg", "src": ["./images/clientInterface1.jpg", "./images/clientInterface2.jpg", "./images/clientInterface3.jpg"], "srcImageCache": "./images/videoVignetteCache.jpg", "fontColor": "black", "html": "<h5 id=\"videoVignetteTitle3\">interface<br><b class=\"redTitle\">clients</b><br>&zwnj;<br>&zwnj;</h5>"},
    { "Type": "w", "Title": "bientôt disponible aussi !", "alt": "web 4", "srcImage": "./images/vignetteBientot.jpg", "src": ["./images/bientot.jpg"], "srcImageCache": "./images/videoVignetteCache.jpg", "fontColor": "white",  "html": "<h5 id=\"videoVignetteTitle4\">un autre<br>de nos sites<br>sera présenté ici<br><b class=\"redTitle\">très prochainement</b></h5>" }
];

$(document).ready(function () {
    initialiseEvents();
    initialiseAnims();

    ////////////////////
    ////contact page////
    ////////////////////
    $(".contactPageGoUp").mouseenter(function () {
        $("#goSimulatorButton").css("background-color", "#eaeaea");
        $("#goSimulatorArrows").attr("src", "images/goUpArrowsWhite.png");
    }).mouseleave(function () {
        $("#goSimulatorButton").css("background-color", "#e63e3d");
        $("#goSimulatorArrows").attr("src", "images/goUpArrows1.png");
    });
});

    /////////////////
    ////main page////
    /////////////////
function initialiseEvents() {
        // quand la souris entre sur un element pour switch vers le Who are we
        $(".linkToPres").mouseenter(function () {
            $("#linkToPresentation").css("color", "#eaeaea");
            $("#rightNavArrow").attr("src", "images/rightArrowsWhite.png");
        })
        .mouseleave(function () {
            $("#linkToPresentation").css("color", "#e63e3d");
            $("#rightNavArrow").attr("src", "images/rightArrows1.png");
        });

    ///////////////////////
    ////who are we page////
    ///////////////////////
    let timeToFadeSwitchPage = 500;

    $(".linkToPres").click(function () {
        $("#titleHomePage").animate({ opacity: 0 }, timeToFadeSwitchPage).queue(function () {
            $("#titleHomePage").css("display", "none");
            $("#whoAreWePage").css("display", "block");
            $("#whoAreWePage").animate({ opacity: 1 }, timeToFadeSwitchPage).dequeue();
        });
    });

    $(".linkToMain").mouseenter(function () {
        $("#leftNavArrow").attr("src", "images/leftArrowsWhite.png");
        $("#goMainButton").css("background-color", "#eaeaea");
    }).mouseleave(function () {
        $("#leftNavArrow").attr("src", "images/leftArrows1.png");
        $("#goMainButton").css("background-color", "#e63e3d");
    }).click(function () {
        $("#whoAreWePage").animate({ opacity: 0 }, timeToFadeSwitchPage).queue(function () {
            $("#titleHomePage").show();
            $("#whoAreWePage").hide();
            $("#titleHomePage").animate({ opacity: 1 }, timeToFadeSwitchPage).dequeue();
        })
    });

    /////////////////////
    ////our work page////
    /////////////////////
    //next previous arrows pour le lecteur d'image//////////
    $("#awpLeftArrow").click(PreviousShowReelElement);
    $("#awpRightArrow").click(NextShowReelElement);

    //play button///////////
    $("#playButton").on({
        mouseenter: function () {
            $("#playButton").stop(true, false);
            $("#playButton").animate({ opacity: 1 }, 300);
        },
        mouseleave: function () {
            $("#playButton").stop(true, false);
            $("#playButton").animate({ opacity: 0.5 }, 300);
        },
        click: function () {
            $("#playButton").attr("src", "images/playButtonActive.png");
            $("#pauseButton").attr("src", "images/pauseButton.png");
        }
    });

    //pause button///////////
    $("#pauseButton").on({
        mouseenter: function () {
            $("#pauseButton").stop(true, false);
            $("#pauseButton").animate({ opacity: 1 }, 300);
        },
        mouseleave: function () {
            $("#pauseButton").stop(true, false);
            $("#pauseButton").animate({ opacity: 0.5 }, 300);
        },
        click: function () {
            $("#pauseButton").attr("src", "images/pauseButtonActive.png");
            $("#playButton").attr("src", "images/playButton.png");
        }
    });

    ///////next previous picture / web player///////
    // awp hover functions
    var awpHoverIn = function () {
        this.stop(true, false).animate({ opacity: 0.9 }, 100);
    };
    var awpHoverOut = function () {
        this.stop(true, false).animate({ opacity: 0.3 }, 100);
    };
    //main
    var awp = Snap("#arrowsWebPlayer");
    var awpLoad = Snap.load("images/webPlayerArrow.svg", function (awpLoaded) {
        awp.append(awpLoaded);
        awp.select("#awpLeftArrow").hover(awpHoverIn, awpHoverOut);
        awp.select("#awpRightArrow").hover(awpHoverIn, awpHoverOut);
        awp.select("#awpLeftArrow").click(PreviousShowReelElement);
        awp.select("#awpRightArrow").click(NextShowReelElement);
        // initial modifications
        awp.selectAll("g").attr({ opacity: 0.3 });
    });

    //switch mouse enter
    $("#videoSwitch").mouseenter(function () {
        $("#videoSwitch").css("color", "#eeeeee");
    });
    $("#webSwitch").mouseenter(function () {
        $("#webSwitch").css("color", "#eeeeee");
    });
    //switch mouse leave
    $("#videoSwitch").mouseleave(function () {
        if (webOrVideo === 'v') {
            $("#videoSwitch").css("color", "#e63e3d");
        } else {
            $("#videoSwitch").css("color", "black");
        }
    });
    $("#webSwitch").mouseleave(function () {
        if (webOrVideo === 'w') {
            $("#webSwitch").css("color", "#e63e3d");
        } else {
            $("#webSwitch").css("color", "black");
        }
    });

}


function initialiseAnims() {
    /////////////////////
    ////our work page////
    /////////////////////

    // switch to video event //
    $("#videoSwitch").click(function () {
        //$("#ourWorkSwitchVideoLine").stop(true, false).animate({ width: '100%' }, 200);
        //$("#ourWorkSwitchWebLine").stop(true, false).animate({ width: '0%' }, 200);
        $("#ourWorkSwitchVideoLine").css({ width: '100%' });
        $("#ourWorkSwitchWebLine").css({ width: '0%' });
        if (webOrVideo === "w") {
        $("#webPicturesContainer").stop(true, false).animate({ opacity: 0 }, 500)
            .queue(function () {
                $("#webPicturesContainer").css("display", "none");
                $("#videoPlayer").css("display", "block").stop(true, false).animate({ opacity: 1 }, 500)
                    .dequeue();
            });
        $("#pauseButton").css("visibility", "hidden");
        $("#playButton").css("visibility", "hidden");
        $("#webSwitch").css("color", "black");
        $("#videoSwitch").css("color", "#e63e3d");
        $("#ourWorkVarTitle").text("vidéo");
        $("#playButton").stop(true, false);
        $("#pauseButton").stop(true, false);
        $("#playButton").animate({ opacity: 0.5 }, 700);
        $("#pauseButton").animate({ opacity: 0.5 }, 700);

            DrawOurWork("v");
        }
    });

    // switch to web event //
    $("#webSwitch").click(function () {
        $("#ourWorkSwitchVideoLine").css({ width: '0%' });
        $("#ourWorkSwitchWebLine").css({ width: '100%' });
        if (webOrVideo === "v") {
        $("#videoPlayer").stop(true, false).animate({ opacity: 0 }, 500)
            .queue(function () {
                $("#videoPlayer").css("display", "none");
                $("#webPicturesContainer").css("display", "block").stop(true, false).animate({ opacity: 1 }, 500)
                    .dequeue();
            });
        $("#videoSwitch").css("color", "black");
        $("#webSwitch").css("color", "#e63e3d");
        $("#ourWorkVarTitle").text("web");

        $("#pauseButton").stop(true, false)
            .animate({ opacity: 0 }, 700)
            .queue(function () {
                $("#pauseButton").attr("src", "images/pauseButton.png");
                $("#pauseButton").css("visibility", "hidden")
                    .dequeue();
            });
        $("#playButton").stop(true, false)
            .animate({ opacity: 0 }, 700)
            .queue(function () {
                $("#playButton").attr("src", "images/playButton.png");
                $("#playButton").css("visibility", "hidden")
                    .dequeue();
            });

            DrawOurWork("w");
        }
        });
    DrawOurWork("i");
}

function GetFormInput(contact, description) {
    var formResult = { video: videoActive ? "Projet vidéo !" : "Pas de vidéo", web: webActive ? "Projet web !" : "Pas de web" };
    return { "contact": contact, "description": description , "simulator": formResult};
}

function CheckStringIsEmpty(value) {
    if (value === null || value === undefined || value === "")
        return true;
    return false;
}

function NextShowReelElement() {
    if (selectedVignet < -1 || selectedVignet > allVignets.length - 1)
        return;
    var size = allVignets[selectedVignet].src.length;
    if (indexShowreelViewer >= size - 1) {
        indexShowreelViewer = 0;
    }
    else {
        indexShowreelViewer++;
    }
    RefreshShowreelViewer();
}

function PreviousShowReelElement() {
    if (selectedVignet < -1 || selectedVignet > allVignets.length - 1)
        return;
    var size = allVignets[selectedVignet].src.length;
    if (indexShowreelViewer <= 0) {
        indexShowreelViewer = size-1;
    }
    else {
        indexShowreelViewer--;
    }
    RefreshShowreelViewer();
}

function RefreshShowreelViewer() {

    if (webOrVideo === "w") {
        $("#webWitness11").attr("src", allVignets[selectedVignet].src[indexShowreelViewer]);
    }
}


// methode pour l'affichage soit des données de vidéo soit de web
function DrawOurWork(switchType) {
    var i = 0;

    if (webOrVideo === switchType)
        return;

    if (switchType === "i")
        webOrVideo = "v";
    else
        webOrVideo = switchType;

    var selectedVignets = allVignets.filter(x => x.Type === webOrVideo);
    DrawFirstVignetData(selectedVignets[0]);

    // on boucle sur un tableau de vignets(soit web soit video)
    // on modifie le html pour afficher les elements contenus dedans
    // et on leurs maps les events de modification
    selectedVignets.forEach(function (element) {
        var j = 0;
        i++;
        if (webOrVideo === "v")
            j = (selectedVignets.length + 1 - i);
        else
            j = i;

        var idElement = "#vignette" + String(i);
        var idSource = "#vignetteSource" + String(i);
        var idVignetteTitle = "#videoVignetteTitle" + String(i);
        var idCache = "#vignetteCache" + String(i);

        $(idElement).unbind();
        $(idElement).stop(true, false);
        $(idElement).delay(150 * j).animate({ opacity: 0 }, 300).queue(function () {
            $(idSource).attr("src", element.srcImage);
            $(idSource).attr("alt", element.alt);
            $(idSource).attr("width", propsVignets.width);
            $(idSource).attr("height", propsVignets.height);

            $(idCache).attr("src", element.srcImageCache);
            $(idCache).attr("alt", propsVignets.altCache);
            $(idCache).attr("width", propsVignets.width);
            $(idCache).attr("height", propsVignets.height);

            $(idVignetteTitle).replaceWith(element.html);
            $(idVignetteTitle).css({ "color": element.fontColor });
            $(idElement).animate({ opacity: 1 }, 700)
                .dequeue();
        });

        if (isTouchScreenAlreadyActivated === false) {
            $(idElement).mouseenter(function () {
                $(idCache).animate({ opacity: 0 }, 700);
                $(idVignetteTitle).delay(300).animate({ opacity: 1 }, 500);
            }).mouseleave(function () {
                $(idCache).stop(true, false);
                $(idVignetteTitle).stop(true, false);
                $(idCache).animate({ opacity: 0.6 }, 400);
                $(idVignetteTitle).animate({ opacity: 0 }, 400);
            });
        }

        $(idElement).click(function () {
            DrawFirstVignetData(element);
        });

    });
}

function DrawFirstVignetData(element) {
            var index = -1;
            for (var i = 0; i < allVignets.length; ++i) {
                if (allVignets[i] === element) {
                    index = i;
                    break;
                }
            }
            if (index < 0 || index > allVignets.length - 1)
                index = 0;
            selectedVignet = i;
            indexShowreelViewer = 0;
            if (webOrVideo === "w") {
                $("#videoPlayer").hide();
                $("#webPicturesContainer").show();
                $("#webWitness11").attr("src", element.src[0]);
                $("#videoRead").text(element.Title);
                if (element.src.length > 1) {
                    $("#awpLeftArrow").show();
                    $("#awpRightArrow").show();
                }
                else {
                    $("#awpLeftArrow").hide();
                    $("#awpRightArrow").hide();
                }
            }
            if (webOrVideo === "v") {
                $("#videoPlayer").show();
                $("#webPicturesContainer").hide();
                $("#videoPlayerSrc").attr("src", element.src[0]);
                $("#videoPlayer").css("z-index", "0");
                $("#ourWorkTitle").css("z-index", "1");
            }
            $("#videoRead").stop(true, false)
                .animate({ opacity: 0 }, 400)
                .queue(function () {
                    $(this).text(element.Title)
                        .dequeue();
                }).animate({ opacity: 1 }, 400);
}

function RenderSensitiveVignets() {
        $(idCache).css("display", "none");
        $(idVignetteTitle).css("opacity", "1");
}

function ValidateSimulator() {
    var contact = $("#SDPUinputContact").val();
    var description = $("#SDPUinputDescription").val();
    if (contact === undefined || contact === null || contact === "" || description === undefined || description === null || description === "") {
        alert("Veuillez remplir l'ensemble des champs svp");
        return;
    }
    $("#SDPUsendButton").css({ "pointerEvents": "none" });
    $("#SDPUsendButton").hide();

    var formResult = { "contact": contact, "description": description, "simulator": { video: videoActive ? "Projet vidéo !" : "Pas de vidéo", web: webActive ? "Projet web !" : "Pas de web" } };
    sendSimulatorResults(formResult, FormSuccess, FormFail);
}

function FormSuccess() {
    $("#SDPUsendButton").stop(true, false).animate({ opacity: 0 }, 500)
        .queue(function () {
            $("#SDPUForm").css("display", "none")
                .dequeue();
        });
    $("#SDPUForm").stop(true, false).animate({ opacity: 0 }, 500)
        .queue(function () {
            $("#SDPUForm").css("display", "none")
                .dequeue();
        });
    $("#SDPUtitles").stop(true, false).animate({ opacity: 0 }, 500)
        .queue(function () {
            $("#SDPUmainTitle")
                .replaceWith(
                    "<h2 id=\"SDPUmainTitle\"><b class=\"redTitle\">merci</b> pour votre confiance !</h2>");
            $("#activeMediasDisplay").css({ "font-family": "robotoR", "font-size": "19px", "color": "#171717" })
                .text("Nous avons bien reçu votre demande et vous recontacterons très prochainement.");
            $("#SDPUtitles").stop(true, false).animate({ opacity: 1 }, 500)
                .queue(function () {
                    $("#SDPUbackground").stop(true, false).animate({ height: '132px' }, 500)
                        .dequeue();
                })
                .dequeue();
        });
}

function FormFail() {
    $("#SDPUsendButton").stop(true, false).animate({ opacity: 0 }, 500)
        .queue(function () {
            $("#SDPUForm").css("display", "none")
                .dequeue();
        });
    $("#SDPUForm").stop(true, false).animate({ opacity: 0 }, 500)
        .queue(function () {
            $("#SDPUForm").css("display", "none")
                .dequeue();
        });
    $("#SDPUtitles").stop(true, false).animate({ opacity: 0 }, 500)
        .queue(function () {
            $("#SDPUmainTitle")
                .replaceWith("<h2 id=\"SDPUmainTitle\">toutes nos <b class=\"redTitle\">excuses</b></h2>");
            $("#activeMediasDisplay").replaceWith(
                "<h1 id=\"activeMediasDisplay\">Votre envoie semble ne pas fonctionner.<br>Vous pouvez réessayer dans quelques minutes ou <a id=\"HDPUformFailureLink\"href=\"#contactPageContainer\">nous contacter directement</a> par mail ou par téléphone.</h1>");
            $("#activeMediasDisplay").css({
                "font-family": "robotoR",
                "font-size": "19px",
                "color": "#171717",
                "line-height": "1.2"
            });
            $("#SDPUmainTitle").css({ "margin-bottom": "25px" });
            $("#HDPUformFailureLink").css({ "color": "#e63e3d", "font-weight": "bold" });
            $("#SDPUtitles").stop(true, false).animate({ opacity: 1 }, 500)
                .queue(function () {
                    $("#SDPUbackground").stop(true, false).animate({ height: '148px' }, 500)
                        .dequeue();
                })
                .dequeue();
        });
}